#!/usr/bin/env pythoh3

'''
Busca una palabra en una lista de palabras
'''




import sortwords

import sys

def search_word(word, words_list):
    found=False
    test_index=0
    i=0
    indexPool=len(sys.argv)-3

    while found == False:

        if i==len(words_list):
            test_index=-1
            print("La palabra buscada no está en la lista")
            break
        test_index = indexPool//2
        if word.lower() == words_list[test_index].lower():
            found=True
        else:
            found=False
            if sortwords.is_lower(word.lower(), words_list[test_index].lower())==True:

                indexPool=len(words_list[:test_index])
            else:
                indexPool=len(words_list[test_index:])
        i=i+1

    return test_index


def main():
    ordered_list=sortwords.sort(sys.argv[4:])
    position=search_word(sys.argv[3], ordered_list)


    ...
    sortwords.show(ordered_list)
    if position !=-1:
        print(position)


if __name__ == '__main__':
    main()
